---
MorpheusModelID: M2001

authors: [L. Edelstein-Keshet, J. Starruss]
contributors: [D. Jahn]

title: CPM Parameter Plane
date: "2021-02-26T18:54:00+01:00"
lastmod: "2021-03-19T18:01:00+01:00"

tags:
- Dimensionless Parameter
- DOI:10.1186/s13628-015-0022-x

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---

>Two-Parameter plane showing all possible regimes of behaviour for a single CPM cell

## Description

The idea is based on [figure 3](https://bmcbiophys.biomedcentral.com/articles/10.1186/s13628-015-0022-x/figures/3) in the [referenced paper](#reference). But here, we use two **dimensionless parameters**:

- Dimensionless **perimeter cost**: `kappa=4*LambP/(LambA*Ra^2)` – on the horizontal axis,
- dimensionless **cell-medium contact energy cost**: `beta= 2*Jmed/(3.14*LambA*Ra^3)` – on the vertical axis,
- **circularity**: `alpha = R_p/R_a` – vary manually.

![Movie of the simulation](movie.mp4)

{{% callout note %}}
**Suggestion:** Try the settings `alpha` = `0.5`, `1`, `2`.
{{% /callout %}}

The results are now consistent with theoretical analysis in the above reference.
However, fine-tuning of the CPM settings was required to obtain the results.

![](plot_02000.png "Plots of the simulation result at time $t = 2000$. One is colored by the `beta` value and the other by the `kappa` value.")

## Reference

>Magno, R., Grieneisen, V. A., Marée, A. F. [The biophysical nature of cells: potential cell behaviours revealed by analytical and computational studies of cell surface mechanics][reference]. *BMC Biophys.* **8**(8): 1-37, 2015.

[reference]: https://doi.org/10.1186/s13628-015-0022-x