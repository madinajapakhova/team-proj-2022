---
MorpheusModelID: M5481

title: "Field-Cell Interaction"

contributors: [V. Ponce, W. de Back, F. El-Hendi]

tags:
- Contributed Examples
- 2D
- PDE
- Diffusion
- Cellular Potts Model
- CPM
- ECM
- Extracellular Matrix
- Field
- Field-Cell
---
>Cells' consumption of cytokine concentration

## Introduction

This example serves to demonstrate the implementation of cells consuming a substrate field, e.g. cytokine. Please also see the built-in docu at `MorpheuML`/`Global`/`Field` and `Global`/`System`/`DiffEqn`. This example was motivated by [Victoria Ponce](/contributor/v.-ponce/) in the [Morpheus user forum](https://groups.google.com/g/morpheus-users/c/fO1EGRLHJ20).

## Description

The logic is like this: In the PDE that describes the reaction and diffusion of the substrate, you include a negative term (i.e. for decrease) that is nonzero where there is a cell and zero where there is no cell.
Steps to reproduce: 
1. Define a `Field` in `Global`, e.g. called `cytokine` with an inital concentration $1.0$ and `Diffusion`.
2. Define a `System` with a differential equation with a negative term, e.g. `-r*cell*cytokine` where `r` is the consumption rate of cells. (Here it is constant, but could also be a cell property to specify that each cell has its own consumption rate.)
3. Define a `Constant` called `cell` which is $0$ everywhere, except for where there is a cell. To do this, specify `cell` in `Global` and set it to $0$. But in `CellType` , the value of the constant is overwritten with a value of $1$.

In this way, the term `-r*cell*cytokine` is negative 'within' cells and zero outside. Therefore, the cytokine concentration is only decreased at locations with cells.

Movie visualising the consumption of cytokine only where the cell is:
![Movie visualising the consumption of cytokine only where the cell is](consumption.mp4)
The panel shows the area covered by the cell (left) and the cytokine concentration over time (right).


