---
MorpheusModelID: M7912

authors: [H. Knutsdottir, C. Zmurchok, D. Bhaskar, E. Palsson, D. D. Nogare, A. B. Chitnis, L. Edelstein-Keshet]
contributors: [F. El-Hendi, U. Rückert, A. Mironenko, S. Gopalakrishnan, L. Brusch]

published_model: reproduction

title: Lateral Line Primordium in 1D
date: "2021-03-31T10:42:00+01:00"
lastmod: "2021-05-26T18:19:00+02:00"

tags:
- GUI
- Lateral Line
- Neuromast
- ParameterSweep
- PDE
- Posterior Lateral Line
- Posterior Lateral Line Primordium
- Primordium
- Python
- Self-organisation
- Zebrafish
---

>Spatio-temporal model of self-organisation inside the zebrafish posterior lateral line primordium

## Introduction

The zebrafish posterior lateral line primordium is the precursor of the neuromasts, which form sensory organs responding to fluid flow. At approximately 22-48 hours post-fertilization, this moving cell cluster consists of approximately 100 cells dynamically subdivided into leading versus trailing phenotypes in a self-organizing process. This self-organizing process relies on feedback loops between ligand and receptor expression and was modeled and analyzed as part of a larger 3D multicellular model in the publication [Knutsdottir *et al.*](#reference).

## Description

The model shown here implements the published partial differential equations Eq. 6 from [Knutsdottir *et al.*](#reference), as well as Eq. 10a,b and Eq. 15a,b from supplementary materials [S1](https://doi.org/10.1371/journal.pcbi.1005451.s001). Parameter values are taken as published in supplementary materials [S4](https://doi.org/10.1371/journal.pcbi.1005451.s004) and [S6](https://doi.org/10.1371/journal.pcbi.1005451.s006). This Morpheus model reproduces the results published in [Figure 5](https://doi.org/10.1371/journal.pcbi.1005451.g005) and [Figure 6](https://doi.org/10.1371/journal.pcbi.1005451.g006) (shown below).

Please note, that the plots for $W_\mathrm{B}$ and $F_\mathrm{B}$ have been omitted in the Morpheus figure, since these variables serve no further purpose and were therefore left out in the model. However, they could be easily added by adding a Morpheus function. Furthermore, note that the right figure in Figure 6 was created with an external Python script. Morpheus generated the necessary data with the `ParameterSweep` module in the GUI. The separate Python code for analysis of the data resulted in the same graph, but is not included here.

![](pcbi.1005451.g005.png "Model results as published by [Knutsdottir *et al.*](#reference) ([Figure 5](https://doi.org/10.1371/journal.pcbi.1005451.g005)). [*CC0 1.0*](https://creativecommons.org/publicdomain/zero/1.0/)")

![](morpheus_figure_5.png "Model results reproduced with this Morpheus model.")

![](pcbi.1005451.g006.png "Model results as published by [Knutsdottir *et al.*](#reference) ([Figure 6](https://doi.org/10.1371/journal.pcbi.1005451.g006)). [*CC0 1.0*](https://creativecommons.org/publicdomain/zero/1.0/)")

![](morpheus_figure_6.png "Model results reproduced with this Morpheus model.")

## Reference

This model is described in the peer-reviewed publication:

>H. Knutsdottir, C. Zmurchok, D. Bhaskar, E. Palsson, D. D. Nogare, A. B. Chitnis, L. Edelstein-Keshet: [Polarization and migration in the zebrafish posterior lateral line system][reference]. *PLOS Comput. Biol.* **13** (4), 1-26, 2017.

[reference]: https://doi.org/10.1371/journal.pcbi.1005451
