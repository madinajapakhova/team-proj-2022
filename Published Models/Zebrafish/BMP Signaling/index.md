---
MorpheusModelID: M2982

authors: [G. H. Soh, A. P. Pomreinke, P. Müller]
contributors: [P. Brusch]

tags: ["PDE"]

title: BMP Signaling
date: "2020-11-21T18:00:00+01:00"
lastmod: "2020-11-21T18:00:00+01:00"
---

>Spatio-temporal model of BMP signaling

## Introduction

Juxtaposed sources of bone morphogenetic protein (BMP) and Nodal signaling molecules induce the formation of a full body axis in zebrafish embryos. Soh *et al.* studied body axis formation in zebrafish by transplanting signaling sources, specifically single clones expressing mouse BMP4 (mBMP4), and thereby inducing a secondary axis. mBMP4 diffuses and forms a morphogen gradient in the host tissue where differential signal transduction kinetics for pSmad2 versus pSmad5 lead to narrow (pSmad2) versus wide (pSmad5) signal activity ranges in response to the same morphogen gradient (mBMP4) as input.

Soh *et al.* used mathematical modeling and simulation in COMSOL to explore the signal activity ranges in the mBMP4-pSmad2-pSmad5 system. The below Morpheus model implements the same partial differential equations (PDEs) and parameter values as published in Suppl. Section "Mathematical modeling" of the [referenced paper](#reference). This simulation reproduces Fig. 3B of the referenced paper. 

## Description

- Units are $\[\text{space}\] = \mathrm{\mu m}$, $\[\text{time}\] = \mathrm{s}$.
- A radial coordinate $x$ is used to optionally plot the solution profiles (temporarily inactivated in the model file, section `Analysis`). 
- Also optionally, time courses of mean concentrations over the domain can be plotted (temporarily inactivated in the model file, section `Analysis`).
- Note, the resulting state at $7200 \ \mathrm{s}$ is a transient state when the mBMP4 field is initialized as zero, a steady state with higher concentration values is approached at ten times longer duration.
- Note, the Morpheus model uses $r = 50 \ \mu \mathrm m$ for the radius of the (blue) source region in the center of the (black) simulation domain with radius $300 \ \mu \mathrm m$. This corresponds to the geometry used in Fig. 3B of the referenced paper.
- The color scales for the two displayed fields are chosen such that red corresponds to maximum pSmad5 of $5000$ and green to maximum pSmad2 of $5$.

![](Soh2020_Fig3B.png "Model results as published by [Soh *et al.*](#reference) ([Figure 3B](https://www.cell.com/cell-reports/fulltext/S2211-1247(20)30365-X?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS221112472030365X%3Fshowall%3Dtrue#fig3)). [*CC BY-NC-ND 4.0*](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)")

![](BMP_signaling.png "Model results reproduced with this Morpheus model.")

## Reference

This model is described in the peer-reviewed publication:

>G. H. Soh, A. P. Pomreinke, P. Müller: [Integration of Nodal and BMP Signaling by Mutual Signaling Effector Antagonism][reference]. *Cell Rep.* **31** (1): 107487, 2020.

[reference]: https://doi.org/10.1016/j.celrep.2020.03.051