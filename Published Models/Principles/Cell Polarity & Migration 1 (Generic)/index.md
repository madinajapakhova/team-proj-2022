---
MorpheusModelID: M2061

authors: [E. G. Rens, L. Edelstein-Keshet]
contributors: [E. G. Rens]

title: "Basic Rac-Rho-ECM Spatial Model"

# Reference details
publication:
  doi: "10.1088/1478-3975/ac2888"
  title: "Cellular Tango: how extracellular matrix adhesion choreographs Rac-Rho signaling and cell movement"
  journal: "Phys. Biol."
  volume: 18
  issue: 6
  page: "066005"
  year: 2021
  original_model: true

tags:
- 1D
- Adhesion
- Cell Deformation
- Cell-ECM Adhesion
- Cell Shape
- Cellular Potts Model
- CPM
- ECM
- Extracellular Matrix
- Front Protrusion
- GTPase
- GTPase Signaling
- Oscillation
- Partial Differential Equation
- PDE
- Persistent Polarity
- Polarity Oscillation
- Protrusion-Retraction
- Rac
- Reaction–Diffusion System
- Rear Retraction
- Rho
- Spatial Pattern
- Spiral Wave

categories:
- DOI:10.1088/1478-3975/ac2888

# order
#weight: 10
---
## Introduction

This paper describes the regimes of behaviour of a 1D spatial (PDE) model for the mutually antagonistic Rac-Rho GTPases, with feedback to and from the extracellular matrix (ECM).

## Description

The [Rac-Rho submodels](/category/doi10.1088/1478-3975/ac2888/) are bistable, and ECM enhances Rho activation. Rac and Rho contribute positive and negative feedback, respectively, to the ECM. The full model has regimes of uniform, polar, random, and oscillatory dynamics.

## Results

The [file listed below](#model) was used to produce [supplementary Figure 1](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888/pdf) showing Model&nbsp;I regimes: Behaviour of Rac for the full 1D spatially distributed version of Model&nbsp;I, Eqs. [2.3a](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888#pbac2888eqn2_3a) and [2.3b](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888#pbac2888eqn2_3b) in the paper. These 1D PDEs are a spatial variant of a basic model previously studied by Holmes *et al.* Here the ECM dynamics is phenomenological, with Rac enhancing and Rho damping the ECM signaling.

![](RensKeshetModel1image.jpg "[Suppl. Fig. 1](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888/pdf) showing Model I regimes ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Rens *et al.***](#reference))")