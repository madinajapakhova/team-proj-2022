---
MorpheusModelID: M7675

authors: [H. Fooladi, P. Moradi, A. Sharifi-Zarchi and
B. Hosein Khalaj]
contributors: [H. Fooladi, L. Brusch]

title: "Human ESC Colony Patterning"
date: "2018-10-31T13:00:00+01:00"
lastmod: "2021-08-26T20:00:00+02:00"

# Reference details
publication:
  doi: "10.1093/bioinformatics/btz201"
  title: "Enhanced Waddington landscape model with cell--cell communication can explain molecular mechanisms of self organization"
  journal: "Bioinformatics"
  volume: 35
  issue: 20
  page: "4081--4088"
  year: 2019
  original_model: true

tags:
- BMP
- Cell-Cell Communication
- Cellular Potts Model
- CPM
- Embryonic Stem Cell
- Gene Regulatory Network
- Human
- Noggin
- Spatio-Temporal Patterning
- Stem Cell
- Tissue Engineering
- Waddington landscape model

categories:
- DOI:10.1093/bioinformatics/btz201
---

## Introduction

The formation and maintenance of three germ layers in colonies of human embryonic stem cells (ESCs) is sensitive to the geometry of the colony. In vitro experiments with micropatterns controlling colony shape have previously addressed the underlying spatio-temporal patterning in [Warmflash et al. 2014](https://doi.org/10.1038/nmeth.3016), [Etoc et al. 2016](https://doi.org/10.1016/j.devcel.2016.09.016) and [Tewary et al. 2017](https://doi.org/10.1242/dev.149658).

This model by [Fooladi *et al.*](#reference) builds upon the three experimental and modeling works above and extends the basic principle of Waddington's landscape by cell-cell communication. The model explores the experimentally observed sensitivity to colony geometry.

## Description

- **Units** in the model are $\[\text{space}\] = \mathrm{\mu m}$, $\[\text{time}\] = \text{second}$.
- **Parameter values** were collected from literature by [Fooladi *et al.*](#reference).
- **Boundary conditions** are no-flux along the domain boundary by default.
- **Domain shape** is defined as binary TIFF image file and mimicks the cross section geometry of the gastrula.
- **Initial condition**: cells are placed within the imported spatial domain shape with approximately 90% confluence.
- The **original** model file was developed with Morpheus version 1.9.3 and was shared by the authors at [GitHub](https://github.com/HFooladi/Self_Organization)
- The **model** file provided [below](#model) has been updated (solver naming, added reference and links to description) such that it works with [Morpheus version 2.2.4 and later](/download/latest/) and **reproduces the original results**.

## Results

Running the [model](#model) for 300 Monte Carlo steps from random initial conditions, self-organizes the BMP and Noggin expression patterns and ligand fields (see titles above each panel) as shown in this movie

![Video of the model simulation.](reproduced_Fig.6d.mp4)

and reproduces the published [Figure 6d](https://academic.oup.com/view-large/figure/164414602/btz201f6.tif) in [Fooladi *et al.*](#reference) as follows.

![Final state of the model simulation.](reproduced_Fig.6d.png "Published model results in Figure 6d are reproduced with this Morpheus model.")

When the upper neck part of the domain shape is widened as in the published Figure 6c (with file Domain_6c.tif provided below), then more interior cell states will develop there with low BMP values around 2 (shown below).

![Final state of the model simulation.](reproduced_Fig.6c.png "Published model results in Figure 6c are reproduced with this Morpheus model.")

