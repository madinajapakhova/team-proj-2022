<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour


This file produced SI Figure 4.

We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project </Details>
        <Title>YREsheetFlat</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="400, 100, 0"/>
            <BoundaryConditions>
                <Condition boundary="x" type="constant"/>
                <Condition boundary="-x" type="noflux"/>
                <Condition boundary="y" type="periodic"/>
                <Condition boundary="-y" type="periodic"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="stoptime" value="1500"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells flooding="true" value="Y">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.05" color="light-blue"/>
                            <Color value="0.1" color="light-red"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot title="Rac1">
                <Cells flooding="true" max="3" min="0" value="R">
                    <ColorMap>
                        <Color value="0" color="blue"/>
                        <Color value="3" color="red"/>
                        <Color value="4" color="yellow"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Cells flooding="true" max="2" min="0" value="E">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="plum"/>
                            <Color value="4" color="blue"/>
                            <Color value="7" color="cyan"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot>
                <Cells flooding="true" value="d.abs">
                    <!--    <Disabled>
        <ColorMap>
            <Color value="0" color="skyblue"/>
            <Color value="0.1" color="violet"/>
            <Color value="0.2" color="salmon"/>
        </ColorMap>
    </Disabled>
-->
                </Cells>
            </Plot>
        </Gnuplotter>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
    <Global>
        <Constant symbol="ky" name="Basal rate of YAP activation" value="0.1"/>
        <Constant symbol="kye" name="E-cadherin-dependent rate of YAP deactivation" value="1.8"/>
        <Constant symbol="Dy" name="Inactivation rate of YAP" value="2"/>
        <Constant symbol="C" name="Initial activation rate of E-cadherin" value="0.9"/>
        <Constant symbol="ke" name="YAP-dependent rate of E-cadherin expression" value="0.9"/>
        <Constant symbol="K" name="Dissociation constant of YAP-WT1 transcriptional constant" value="1"/>
        <Constant symbol="De" name="Inactivation rate of E-cadherin" value="1"/>
        <Constant symbol="h" name="Hill coefficient for E-cadherin" value="3"/>
        <Constant symbol="kr" name="YAP-dependent rate of Rac1 expression" value="1"/>
        <Constant symbol="Kr" name="Michaelis-Menten-like constant for Rac1" value="0.5"/>
        <Constant symbol="Dr" name="Degradation rate of Rac1" value="0.5"/>
        <Constant symbol="n" name="Hill coefficient for Rac1" value="6"/>
        <Constant symbol="alphaR" name="Rac activation fraction" value="1"/>
        <Constant symbol="kyr" name="Rac1-dependent rate of YAP activation" value="1.8"/>
        <!--    <Disabled>
        <Constant value="2" symbol="A2" name="basal adhesion">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="A2" name="Max E-cadherin adhesion constant" value="12">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant symbol="A3" name="E-cadherin half &quot;saturation&quot; " value="0.85"/>
        <Constant symbol="C1" name="basal migration" value="0.4"/>
        <Constant symbol="C2" name="Max Rac1 migration constant" value="4"/>
        <Constant symbol="C3" name="Rac1 half &quot;saturation&quot;" value="3"/>
        <Constant symbol="tlim" name="max time for initial leader cells to emerge" value="100"/>
        <Constant symbol="frac" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it" value="0.2"/>
        <!--    <Disabled>
        <Constant value="0.5" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="1.2">
            <Annotation>Control</Annotation>
        </Constant>
        <!--    <Disabled>
        <Constant value="2" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>OE</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="Rtot" name="Total Rac1 (active (R) + inactive)" value="5"/>
        <Constant symbol="shareprob" name="parameter that determines probability of Cr spread in each time step" value="0.02"/>
        <Variable symbol="E" name="E-cadherin" value="0.0"/>
        <Variable symbol="Cr" name="Basal Rac1 activation rate" value="0.0"/>
        <!--    <Disabled>
        <Variable value="0.0" symbol="x_edge2"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output mapping="minimum" symbol-ref="x_edge2"/>
        </Mapper>
    </Disabled>
-->
    </Global>
    <CellTypes>
        <CellType class="medium" name="medium"/>
        <CellType class="biological" name="dividingcell">
            <VolumeConstraint strength="1" target="50"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint mode="aspherity" strength="1" target="1"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion name="cell migration" strength="C1+C2*R/(C3+R)" direction="1, 0.0, 0.0"/>
            <Property symbol="Y" name="YAP" value="0"/>
            <Property symbol="E" name="E-cadherin" value="10"/>
            <Property symbol="R" name="Rac1" value="0"/>
            <Property symbol="Cr" value="0.001"/>
            <Property symbol="dist" name="distance from right domain edge" value="0.0"/>
            <Property symbol="avspeed" name="Sum of instantaneous speeds each time step (times 100)" value="0.0"/>
            <Property symbol="truavspeed" name="Average Speed * 100" value="0.0"/>
            <Property symbol="avspeed2" name="Sum of instantaneous speeds over final 200 time steps (times 100)" value="0.0"/>
            <Property symbol="truavspeed2" name="average speed over final 200 time steps (times 100)" value="0.0"/>
            <Property symbol="M" name="Contact with medium" value="0.0"/>
            <Property symbol="neigh" name="number of neighbour cells" value="0.0"/>
            <Property symbol="av" name="average neighbourhood Cr" value="0.0"/>
            <PropertyVector symbol="d" name="speed" value="0.0, 0.0, 0.0"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="Cr" scaling="cell"/>
                <Output mapping="average" symbol-ref="av"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn name="Equation for YAP" symbol-ref="Y">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for E-cadherin" symbol-ref="E">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for Rac1" symbol-ref="R">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <!--    <Disabled>
        <Rule name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr" symbol-ref="Cr">
            <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)*0+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
        </Rule>
    </Disabled>
-->
                <Rule name="distance from right domain edge" symbol-ref="dist">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds each time step (times 100)" symbol-ref="avspeed">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule name="average speed over the simulation (times 100)" symbol-ref="truavspeed">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds over final 100 time steps (times 100)" symbol-ref="avspeed2">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule name="average speed over 100 time steps (times 100)" symbol-ref="truavspeed2">
                    <Expression>avspeed2/200</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="Cr">
            <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*0.1*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>size.x/10)</Expression>
        </Rule>
    </Disabled>
-->
            </System>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population size="1" type="dividingcell" name="Initialize cell sheet at left edge of the domain">
            <!--    <Disabled>
        <InitRectangle mode="regular" number-of-cells="70">
            <Dimensions origin="size.x/100, 0, 0" size="size.x/100, size.y, size.z"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle mode="regular" number-of-cells="70">
                <Dimensions size="4.0, size.y, 0.0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type1="dividingcell" value="30" type2="dividingcell">
                <AddonAdhesion name="Adhesive strength" strength="5" adhesive="A2*E/(A3+E)"/>
            </Contact>
            <Contact type1="dividingcell" value="12" type2="medium"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
